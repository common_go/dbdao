module gitee.com/common_go/dbdao

go 1.15

//replace gitee.com/common_go/config => /Users/wll/Go/src/tal_udc_go/config

require (
	gitee.com/common_go/config v0.0.1
	gitee.com/common_go/logger v0.0.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/go-xorm/xorm v0.7.9
	github.com/spf13/cast v1.5.0
	xorm.io/core v0.7.2-0.20190928055935-90aeac8d08eb
)
