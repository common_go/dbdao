// +----------------------------------------------------------------------
// |  
// |  
// | 
// +----------------------------------------------------------------------
// | Copyright (c) udc All rights reserved.
// +----------------------------------------------------------------------
// | Author: wanglele <wanglele@tal.com>
// +----------------------------------------------------------------------
// | Date: 2021/6/30 2:30 下午
// +----------------------------------------------------------------------
package dbdao

import (
	"gitee.com/common_go/config"
	"os"
	"testing"
)

func TestDb(t *testing.T) {
	dir, _ := os.Getwd()
	config.SetConfigPath(dir + "/conf/conf.ini")

	db := GetDbInstance("udc")

	masterEngine := db.Engine.Master()
	results, err := masterEngine.Query("select 123 as name")
	if err != nil {
		t.Error("fail", err)
	}

	if len(results) == 1 && string(results[0]["name"]) == "123" {
		t.Log("pass")
	} else {
		t.Error("fail")
	}
}
